function aggiornaTabellaPersone() {
    $.ajax({
      url: "http://localhost:8888/persona/",
      type: "GET",
      dataType: "json",
      success: function (risultato) {
        stampaArrayPersone(risultato);
      },
      error: function (errore) {
        console.log("Sono nel ramo di Error");
        console.log(errore);
      },
    });
  }

  
function stampaArrayPersone(varRisultato) {
    var outTabella = "";
    for (var i = 0; i < varRisultato.length; i++) {
      outTabella += "<tr>";
      outTabella += " <td>" + varRisultato[i].id + "</td>";
      outTabella += " <td>" + varRisultato[i].nome + "</td>";
      outTabella += " <td>" + varRisultato[i].cognome + "</td>";
      outTabella += " <td>" + varRisultato[i].cod_fis + "</td>";
      outTabella +=
        "<td><button type='button' class='btn btn-danger' onclick='eliminaProdotto(" +
        varRisultato[i].id +
        ")'>Cancella</button></td>";
      outTabella +=
        "<td><button type='button' class='btn btn-primary' onclick='modaleModificaProdotto(" +
        varRisultato[i].id +
        ")'>Modifica</button></td>";
      outTabella += "</tr>";
    }
  
    document.getElementById("contenuto-tabella-persone").innerHTML = outTabella;
  }

  

function inserisciPersona() {
    var perNome = document.getElementById("input_nome").value;
    var perCogn = document.getElementById("input_cognome").value;
    var perCodf = document.getElementById("input_codfis").value;
  
    if(perNome == ""){
        alert("Il campo nome non può essere vuoto!");
        $("#input_nome").focus();
        return;
    }

    var persona = {
      nome: perNome,
      cognome: perCogn,
      cod_fis: perCodf,
    };
  
    $.ajax({
      url: "http://localhost:8888/persona/inserisci",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(persona),
      dataType: "json",
      success: function (risultato) {
        if (risultato.id) {
            alert("Persona inserita");
            window.location.replace("listaPersone.html");
        } else {
            alert("Errore di inserimento");
        }
      },
      error: function (errore) {
        console.log(errore);
      },
    });
  }
  
  //TODO: MOdifica ed ELiminazione
  
  $(document).ready(function () {

    if(document.getElementById("contenuto-tabella-persone") != null){
        aggiornaTabellaPersone();
    }

  });