function aggiornaSelectPersone(){
    $.ajax({
        url: "http://localhost:8888/persona/",
        type: "GET",
        dataType: "json",
        success: function (risultato) {
            
            var stringaOut = "";
            for(var i=0; i<risultato.length; i++){
                stringaOut += "<option value='" + risultato[i].id + "'>" + risultato[i].nome + ", " + risultato[i].cognome + "</option>";
            }

            $("#select_persona").html(stringaOut);

        },
        error: function (errore) {
          console.log("Sono nel ramo di Error");
          console.log(errore);
        },
      });
}

function inserisciCarta(){
    var carNego = $("#input_negozio").val();
    var carNume = $("#input_numero").val();
    var carProp = $("#select_persona").val();

    // console.log(carNego);
    // console.log(carNume);
    // console.log(carProp);

    var carta = {
        negozio: carNego,
        numero: carNume
    }

    $.ajax({
        url: "http://localhost:8888/carta/inserisci/" + carProp,
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(carta),
        dataType: "json",
        success: function(risultato){
            console.log(risultato);
        },
        error: function(errore){
            console.log(errore);
        }
    })
}


$(document).ready(
    function(){

        aggiornaSelectPersone();

    }
)